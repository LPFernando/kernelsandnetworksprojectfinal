import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.math.BigInteger;

public class WRRBalanceador extends UnicastRemoteObject implements LoadBalancer, Runnable {

	private List<String> servidores = null;
	private List<Integer> pesos;
	private HashMap<String, Integer> indiceServidor;
	private ConcurrentLinkedQueue<ISolicitud> solicitudes = null;
	private int tamanioPaso = 0;
	private boolean servidoresIniciados = false;

	protected WRRBalanceador() throws RemoteException {
		super();
	}

	public WRRBalanceador(int puerto) throws RemoteException,IOException {
		this.servidores = new ArrayList<>();
		this.pesos = new ArrayList<>();
		this.indiceServidor = new HashMap<>();
    this.solicitudes = new ConcurrentLinkedQueue<>();
    
		if (System.getSecurityManager() == null) {
			System.class.getResource("/politicas/java.policy").toString();
			System.setProperty("java.security.policy", System.class.getResource("/politicas/java.policy").toString());
			System.setSecurityManager(new SecurityManager());
		}
		try {
			System.out.println("rmi://localhost:" + puerto + "/LoadBalancer");
			Naming.rebind("rmi://localhost:" + puerto + "/LoadBalancer", this);
			System.out.println("Estableciendo load balancer con exito");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	private static int gcd(int a, int b) {
		BigInteger b1 = new BigInteger(""+a);
		BigInteger b2 = new BigInteger(""+b);
    BigInteger gcd = b1.gcd(b2);
    
		return gcd.intValue();
	}

	private void calcularTamanioPaso() {
		int tempTamanioPaso = pesos.get(0);
		for (int i = 1; i < pesos.size(); i++)
			tempTamanioPaso = gcd(tempTamanioPaso, pesos.get(i));
		tamanioPaso = tempTamanioPaso;
	}

	@Override
	public void RegistrarServidor(IServidor servidor) throws RemoteException {
		servidores.add(servidor.getID());
		pesos.add(servidor.getVelocidadProcesamiento());

		indiceServidor.put(servidor.getID(), servidores.size() - 1);

		if (servidores.size() == 1)
			tamanioPaso = servidor.getVelocidadProcesamiento();
		else
			tamanioPaso = gcd(tamanioPaso, servidor.getVelocidadProcesamiento());
		servidoresIniciados = true;
	}

	@Override
	public synchronized void NoregistrarServidor(IServidor servidor) throws RemoteException {
		servidores.remove(servidor.getID());
		int indice = indiceServidor.get(servidor.getID());
		pesos.set(indice, -1);
		indiceServidor.remove(servidor.getID());
	}

	public void agregarSolicitud(ISolicitud solicitud) throws RemoteException {
		solicitudes.add(solicitud);
	}

	public void cambiarWeight(IServidor servidor) throws RemoteException {
		int indice = indiceServidor.get(servidor.getID());
		pesos.set(indice, servidor.getVelocidadProcesamiento());
		calcularTamanioPaso();
	}

	@Override
	public void run() {
		int turnoServidor = 0;
		int pesoActualServidor = 0;
		while (servidores.size() > 0 || !servidoresIniciados) {
			if (!solicitudes.isEmpty() && !servidores.isEmpty()) {
				//System.out.println(pesoActualServidor);
				if (turnoServidor >= servidores.size())
          turnoServidor = turnoServidor % servidores.size();
          
        String URLservidorActual = String.format("rmi://localhost:1099/%s", servidores.get(turnoServidor));
        
				try {
					IServidor servidorActual = (IServidor) Naming.lookup(URLservidorActual);
					ISolicitud solicitudActual = solicitudes.peek();
					if (servidorActual.isHabilitado()) {
						solicitudes.poll();
						servidorActual.agregarSolicitud(solicitudActual);
					}
					else {

						pesoActualServidor = Integer.MIN_VALUE;
					}
				} catch (NotBoundException|MalformedURLException|RemoteException e) {
					e.printStackTrace();
        }
        
        pesoActualServidor += tamanioPaso;
        
				if (!servidores.isEmpty() && (pesoActualServidor >= pesos.get(turnoServidor % servidores.size())) || (pesoActualServidor < 0)) {
					pesoActualServidor = 0;
					turnoServidor++;
				}
			}
		}
		System.out.println("apagando Load balancer");
	}
}