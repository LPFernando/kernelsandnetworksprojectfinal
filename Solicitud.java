import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Solicitud extends java.rmi.server.UnicastRemoteObject implements ISolicitud {

	private String ID;
	private String toIP;
	private int toPuerto = 1099;
	private String url = null;
	private int tiempoProcesamiento = 0;
	private LoadBalancer balanceador;
	private int prioridad;

	public Solicitud(String ID, String toIP, int puerto, int tProcesamiento, int prioridad) throws RemoteException {
		this.ID = ID;
		this.toIP = toIP;
		this.toPuerto = puerto;
		url = String.format("rmi://%s:%d/LoadBalancer", toIP, toPuerto);
		this.tiempoProcesamiento = tProcesamiento;
		this.prioridad=Math.min(prioridad, 10);

		if ( System.getSecurityManager() == null ) {
			System.setProperty("java.security.policy", System.class.getResource("/politicas/java.policy").toString());
			System.setSecurityManager( new SecurityManager() );
		}
		try {
			this.balanceador = (LoadBalancer) Naming.lookup(url);
			this.balanceador.agregarSolicitud(this);
		}
		catch (RemoteException|NotBoundException|MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public int getTiempoProcesamiento() throws RemoteException {
		return tiempoProcesamiento;
	}

	public String getID() throws RemoteException {
		return ID;
	}

	@Override
	public int getPrioridad() throws RemoteException {
		return prioridad;
	}
}
