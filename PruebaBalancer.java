import java.io.FileReader;

//javac *.java
//start rmiregistry
//java PruebaBalancer w debug pTiempo
//java PruebaBalancer w debug pTiempo > resultados/wrr2_out.txt

import java.io.BufferedReader;

public class PruebaBalancer {

	public static void main(String[] args) {

		boolean debug = false;

		try {

			switch (args[0]) {
				case "w":
					WRRBalanceador wrr = new WRRBalanceador(1099);
					new Thread(wrr).start();
					break;
			}

			if(args.length > 1 && args[1].equals("debug")){
				//System.out.println("AEA debug");
				debug = true;
			}

			Thread.sleep(2000);

			String [] nombreServidoresCiudades = {"Lima","Arequipa","Cusco"};
			int [] velocidad = {2, 4, 6};
			int [] caps = {10, 10, 10};
			Servidor ciudad = null;

			for (int i = 0; i < velocidad.length; i++) {

				ciudad = new Servidor("127.0.0.1", 1099, nombreServidoresCiudades[i], velocidad[i], caps[i]);

				if(debug){
					ciudad.alternarDebug();
					System.out.println("Servidor "+nombreServidoresCiudades[i]+" cambiando capacidad de "+caps[i]+" a "+caps[i]);
					System.out.println("Servidor "+nombreServidoresCiudades[i]+" cambia velocidad de procesamiento de "+velocidad[i]+" a "+velocidad[i]);
				}

				new Thread(ciudad).start();
			}

			Thread.sleep(2000);

			FileReader file = null;

			if(args.length > 2 && args[2].equals("pTiempo")){

				file = new FileReader("datosTiempoPequenio.csv");

			} else if(args.length > 2 && args[2].equals("gTiempo")){

				file = new FileReader("datosTiempoGrande.csv");

			} else {
				file = new FileReader("datosTiempoNormal.csv");
			}

			System.out.println("********************************************************");
			BufferedReader in = new BufferedReader(file);
			System.out.println("Creando Solicitudes");
			String linea;
			int i = 0;
			System.out.println("********************************************************");

			while ((linea = in.readLine())!=null){
				String[] datos = linea.split(",");
				new Solicitud("Solicitud " + i, "127.0.0.1", 1099, Integer.parseInt(datos[0]), Integer.parseInt(datos[2]));
				Thread.sleep(Integer.parseInt(datos[1]));
				i++;
			}
			} catch (Exception e) {
				e.printStackTrace();
			}


	}
}
