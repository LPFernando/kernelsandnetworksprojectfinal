import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Servidor extends java.rmi.server.UnicastRemoteObject implements IServidor, Runnable {

	private LoadBalancer balanceador;
	private  boolean isHabilitado = true;
	private String ID;
	private int velocidadProcesamiento = 1;
	private String IPBalanceador;
	private int puertoRegistro = 1099;
	private int solicitudesProcesadas = 0;
	private int tiempoProcesamiento = 0;
	private int tiempoActividad = 0;
	private int tiempoAntesPrimeraActividad = 0;
	private int tiempoInactividad = 0;
	private int capacidad = 1;
	private ConcurrentLinkedQueue<ISolicitud> solicitudes = null;

	private boolean debug = false;

	protected Servidor() throws RemoteException {
		super();
	}

	public Servidor(String ipbalanceador, int puertoregistro, String ID, int velocidad, int capacidad)  throws RemoteException, MalformedURLException, NotBoundException{
		this.IPBalanceador = ipbalanceador;
		this.puertoRegistro = puertoregistro;
		this.ID = ID;
		this.velocidadProcesamiento = velocidad;
    this.solicitudes = new ConcurrentLinkedQueue<>();
    this.capacidad = capacidad;

		if (System.getSecurityManager() == null) {
			System.class.getResource("/politicas/java.policy").toString();
			System.setProperty("java.security.policy", System.class.getResource("/politicas/java.policy").toString());
			System.setSecurityManager(new SecurityManager());
		}

		try {
			System.out.println("********************************************************");
			String url = String.format("rmi://localhost:%d/%s", puertoRegistro, ID);
			System.out.println(url);
			Naming.rebind(url, this);
			System.out.printf("Servidor %s exitosamente levantado\n", ID);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isHabilitado() {
		return isHabilitado;
	}

	@Override
	public synchronized void agregarSolicitud(ISolicitud solicitud) throws RemoteException {
		solicitudes.add(solicitud);
		if (solicitudes.size() >= capacidad)
			isHabilitado = false;
	}

	@Override
	public String getID() throws RemoteException {
		return ID;
	}

	@Override
	public int getVelocidadProcesamiento() throws RemoteException {
		return velocidadProcesamiento;
	}

	public void alternarDebug() {
		this.debug = !this.debug;
	}

	private boolean procesarSolicitud(ISolicitud solicitud) throws RemoteException {

		int tts = (int) Math.ceil( (double) solicitud.getTiempoProcesamiento() / (double) velocidadProcesamiento);

		try {
			Thread.sleep(tts);
			tiempoProcesamiento += tts;
			tiempoActividad += tts;
			solicitudesProcesadas += 1;
			return true;

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void cambiarCapacidad(double cambio) throws RemoteException {
		int capacidadAnterior = capacidad;
		capacidad = (int) Math.ceil(cambio) + capacidad;

		if (capacidad <= 0)
			capacidad = 1;

		System.out.printf("Servidor %s cambiando capacidad de %d a %d (%d procesados)\n", ID, capacidadAnterior, capacidad, solicitudesProcesadas);

		// Retornamos las solicitudes que estan por encima de la capacidad del load balancer
		if (solicitudes.size() > capacidad) {
			isHabilitado = false;
			ConcurrentLinkedQueue<ISolicitud> solRestantes = new ConcurrentLinkedQueue<>();

			int cont = 0;
			
			for (ISolicitud sol : solicitudes) {
				if (cont < capacidad)
					solRestantes.add(sol);
				else
					balanceador.agregarSolicitud(sol);
				cont++;
			}

			System.out.println("Total espera: "+cont);

			solicitudes = solRestantes;
		}
	}

	private void hacerNoDisponible(int tiempo) throws InterruptedException {
		System.out.printf("Servidor %s no disponible por %d ms (%d procesados)\n", ID, tiempo, solicitudesProcesadas);
		Thread.sleep(tiempo);
		tiempoInactividad += tiempo;
	}

	private void cambiarVelocidadProcesamiento(double factor) throws RemoteException {
		int velocidadProcesamientoAnterior = velocidadProcesamiento;
		velocidadProcesamiento = (int) Math.ceil(velocidadProcesamiento * factor);

		if (velocidadProcesamiento <= 0)
			velocidadProcesamiento = 1;
		balanceador.cambiarWeight(this);

		System.out.printf("Servidor %s cambia velocidad de procesamiento de %d a %d (%d procesados)\n", ID, velocidadProcesamientoAnterior, velocidadProcesamiento, solicitudesProcesadas);
		System.out.println("********************************************************");
	}

	private void efectoAleatorio() throws RemoteException, InterruptedException {

		Random random = new Random();
		
		if (random.nextInt(100) == 0)
			cambiarCapacidad(random.nextGaussian() * 4);

		if (random.nextInt(100) == 0)
			cambiarVelocidadProcesamiento(Math.abs(random.nextGaussian() + 1));
	}

	@Override
	public void run() {
		try {

			if (System.getSecurityManager() == null) {
			    System.setProperty("java.security.policy", System.class.getResource("/politicas/java.policy").toString());
			    System.setSecurityManager( new SecurityManager() );
			}

			this.balanceador = (LoadBalancer) Naming.lookup("rmi://" + IPBalanceador + ":" + puertoRegistro + "/LoadBalancer");
			this.balanceador.RegistrarServidor(this);

			System.out.printf("Servidor %s registrado\n", ID);

			while (true) {
				if (!solicitudes.isEmpty()) {

					ISolicitud solicitudActual = solicitudes.peek();

					if(debug){
						System.out.println(ID + " tamanio cola: "+solicitudes.size()+", solicitudes procesadas: "+solicitudesProcesadas);
					}

					if (this.procesarSolicitud(solicitudActual)) {
						solicitudes.poll();
						if (solicitudes.size() <= capacidad)
							isHabilitado = true;
						this.efectoAleatorio();
					}
				}
				else {
					Thread.sleep(100);
					if (tiempoProcesamiento > 0)
						tiempoActividad += 100;
					else
						tiempoAntesPrimeraActividad += 100;
				}
				if (tiempoActividad > 200000 || tiempoAntesPrimeraActividad > 150000)
					break;
			}

			System.out.printf("Servidor %s tiempo total de trabajo %d, tiempo actividad %d, tiempo inactividad %d, procesados totales %d\n", ID, tiempoProcesamiento, tiempoActividad, tiempoAntesPrimeraActividad, solicitudesProcesadas);
			System.out.printf("Apagando servidor %s\n", ID);
			this.balanceador.NoregistrarServidor(this);
		}
		catch (MalformedURLException|RemoteException|SecurityException|NotBoundException|InterruptedException e) {
			e.printStackTrace();
		}
	}


}
