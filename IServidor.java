import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServidor extends Remote {

	void agregarSolicitud(ISolicitud solicitud) throws RemoteException;
	boolean isHabilitado() throws RemoteException;
    String getID () throws RemoteException;
    int getVelocidadProcesamiento() throws RemoteException;
    
}
