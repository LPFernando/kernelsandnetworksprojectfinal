import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LoadBalancer extends Remote {

	public void RegistrarServidor(IServidor servidor) throws RemoteException;
	public void NoregistrarServidor(IServidor servidor) throws RemoteException;
	public void agregarSolicitud(ISolicitud solicitud) throws RemoteException;
	public void cambiarWeight(IServidor servidor) throws RemoteException;
		
}
