import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISolicitud extends Remote {

    String getID () throws RemoteException;
    int getTiempoProcesamiento() throws RemoteException;
    int getPrioridad() throws RemoteException;
}
