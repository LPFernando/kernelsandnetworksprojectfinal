import sys
import matplotlib.pyplot as plt
import re

#python GraficasEnPython/generarGraficos.py resultados/wrr_out.txt

file = open(sys.argv[1], "r")

datos = file.read().splitlines()

salida_servidor = re.compile("([a-zA-Z0-9]+) tamanio cola: ([0-9]+), solicitudes procesadas: ([0-9]+)")
cambio_capacidad = re.compile("Servidor ([a-zA-Z0-9]+) cambiando capacidad de ([0-9]+) a ([0-9]+).*")
cambio_velocidad = re.compile("Servidor ([a-zA-Z0-9]+) cambia velocidad de procesamiento de ([0-9]+) a ([0-9]+).*")

solicitudes_servidor = {}
procesadas_servidor = {}
capacidad_servidor = {}
velocidad_servidor = {}

marca_servidor = 0

for linea in datos:
    if salida_servidor.match(linea):
        temp = linea.split()
        servidor = temp[0]
        sol = temp[2]
        proc = temp[4]

        server = salida_servidor.match(linea).group(1)
        sol = salida_servidor.match(linea).group(2)
        proc = salida_servidor.match(linea).group(3)

        if servidor in solicitudes_servidor.keys():
            solicitudes_servidor[servidor][marca_servidor] = sol
        else:
            solicitudes_servidor[servidor] = {marca_servidor: sol}

        if servidor in procesadas_servidor.keys():
            procesadas_servidor[servidor][marca_servidor] = proc
        else:
            procesadas_servidor[servidor] = {marca_servidor: proc}

        marca_servidor += 1

    if cambio_capacidad.match(linea):
        print(linea)
        servidor = cambio_capacidad.match(linea).group(1)
        comienzo = cambio_capacidad.match(linea).group(2)
        final = cambio_capacidad.match(linea).group(3)

        if servidor in capacidad_servidor.keys():
            capacidad_servidor[servidor][marca_servidor] = comienzo
            capacidad_servidor[servidor][marca_servidor+1] = final
        else:
            capacidad_servidor[servidor] = {marca_servidor: comienzo, marca_servidor+1: final}

    if cambio_velocidad.match(linea):
        print(linea)
        servidor = cambio_velocidad.match(linea).group(1)
        comienzo = cambio_velocidad.match(linea).group(2)
        final = cambio_velocidad.match(linea).group(3)

        if servidor in velocidad_servidor.keys():
            velocidad_servidor[servidor][marca_servidor] = comienzo
            velocidad_servidor[servidor][marca_servidor+1] = final
        else:
            velocidad_servidor[servidor] = {marca_servidor: comienzo, marca_servidor+1: final}

estilos = ["-","--",":"]

plt.figure(1)
plt.subplot()
cont = 0
for servidor in procesadas_servidor:
    sort = sorted(procesadas_servidor[servidor].items())
    x,y = zip(*sort)
    plt.plot(x, y, estilos[cont], label=servidor)
    cont += 1
plt.legend(loc="upper left")
plt.title("Solicitudes procesadas en el tiempo")
plt.savefig("GraficosEnPython/Solicitudes procesadas en el tiempo.png")

plt.figure(2)
plt.subplot()
cont = 0
for servidor in solicitudes_servidor:
    sort = sorted(solicitudes_servidor[server].items())
    x,y = zip(*sort)
    plt.plot(x, y, estilos[cont], label=servidor)
    cont += 1
plt.legend(loc="upper left")
plt.title("Espera de solicitudes en el tiempo")
plt.savefig("GraficosEnPython/Espera de solicitudes en el tiempo.png")

plt.figure(3)
plt.subplot()
cont = 0
for servidor in capacidad_servidor:
    sort = sorted(capacidad_servidor[server].items())
    sort.append((marca_servidor,sort[-1][1]))
    x,y = zip(*sort)
    plt.plot(x, y, estilos[cont], label=servidor)
    cont += 1
plt.legend(loc="upper left")
plt.title("Capacidad del servidor en el tiempo")
plt.savefig("GraficosEnPython/Capacidad del servidor en el tiempo.png")

plt.figure(4)
plt.subplot()
cont = 0
for servidor in velocidad_servidor:
    sort = sorted(velocidad_servidor[servidor].items())
    sort.append((marca_servidor,sort[-1][1]))
    x,y = zip(*sort)
    plt.plot(x, y, estilos[cont], label=servidor)
    cont += 1
plt.legend(loc="upper left")
plt.title("Velocidad del servidor en el tiempo")
plt.savefig("GraficosEnPython/Velocidad del servidor en el tiempo.png")

plt.show()
